/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cours.main;

import com.cours.entities.Personne;
import com.cours.factory.SingletonFactory;
import com.cours.singletons.AbstractSingleton;
import com.cours.singletons.CsvSingleton;
import com.cours.singletons.JsonSingleton;
import com.cours.singletons.XmlSingleton;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class MainSingleton {

    private static final Log log = LogFactory.getLog(MainSingleton.class);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        AbstractSingleton mySingleton = null;

        CsvSingleton csv = CsvSingleton.getInstance();
        for (Personne p: csv.getPersonnes()) {
            System.out.println(p.toString());
        }
        System.out.println("---------------------------------------------------------");
        //csv.getPersonnes().clear();
        JsonSingleton json = JsonSingleton.getInstance();
        for (Personne p: json.getPersonnes()) {
            System.out.println(p.toString());
        }
        System.out.println("---------------------------------------------------------");
        //json.getPersonnes().clear();
        XmlSingleton xml = XmlSingleton.getInstance();
        for (Personne p: xml.getPersonnes()) {
            System.out.println(p.toString());
        }
        //xml.getPersonnes().clear();
        for (Personne p: xml.getPersonnes()) {
            System.out.println(p.toString());
        }

        System.out.println("test singletone Fcatory");
        /* test singletone Fcatory */
        AbstractSingleton abstractSingletonCsv = SingletonFactory
                .getFactory(SingletonFactory.FactorySingletonType.CSV_SINGLETON_FACTORY);
        System.out.println("---------------------------------------------------------");
        //csv.getPersonnes().clear();
        for (Personne p: abstractSingletonCsv.getPersonnes()) {
            System.out.println(p.toString());
        }
        AbstractSingleton abstractSingletonJson = SingletonFactory
                .getFactory(SingletonFactory.FactorySingletonType.JSON_SINGLETON_FACTORY);
        System.out.println("---------------------------------------------------------");
        //csv.getPersonnes().clear();
        for (Personne p: abstractSingletonJson.getPersonnes()) {
            System.out.println(p.toString());
        }
        AbstractSingleton abstractSingletonXml = SingletonFactory
                .getFactory(SingletonFactory.FactorySingletonType.XML_SINGLETON_FACTORY);
        System.out.println("---------------------------------------------------------");
        //csv.getPersonnes().clear();
        for (Personne p: abstractSingletonXml.getPersonnes()) {
            System.out.println(p.toString());
        }
    }
}
