package com.cours.entities;

import com.cours.utils.Constants;


public class Personne {
    private int idPersonne;
    private String prenom;
    private String nom;
    private Double poids;
    private Double taille;
    private String rue;
    private String ville;
    private String codePostal;

    public Personne(int id, String prenom, String nom, Double poids, Double taille
            , String rue, String ville, String code){
        this.idPersonne = id;
        this.prenom = prenom;
        this.nom = nom;
        this.poids = poids;
        this.taille = taille;
        this.rue = rue;
        this.ville = ville;
        this.codePostal = code;
    }

    public Double getImc(){
        return this.poids / Math.pow(this.taille, 2);
    }

    public Boolean isMaigre(){
        Double imc = this.getImc();
        return imc > Constants.LIMITE_INF_MAIGRE && imc <= Constants.LIMITE_SUP_MAIGRE;
    }

    public Boolean isSurPoids (){
        Double imc = this.getImc();
        return imc > Constants.LIMITE_INF_SURPOIDS && imc <= Constants.LIMITE_SUP_SURPOIDS;
    }

    public Boolean isObese  (){
        Double imc = this.getImc();
        return imc > Constants.LIMITE_SUP_SURPOIDS;
    }


    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        res.append("id: ").append(this.idPersonne).append(" nom: ").append(this.nom)
                .append(" prenom: ").append(this.prenom).append(" taille: ")
                .append(this.taille).append(" poids: ").append(this.poids)
                .append(" code: ").append(this.codePostal).append(" rue: ")
                .append(this.rue).append(" ville: ").append(this.ville);
        return res.toString();
    }

    @Override
    public boolean equals(Object obj) {
        Personne p = (Personne)obj;
        return p.idPersonne == idPersonne && p.prenom.equals(prenom) &&
                p.nom.equals(nom) && p.taille.equals(taille) &&
                p.poids.equals(poids) && p.codePostal.equals(codePostal) &&
                p.ville.equals(ville) && p.rue.equals(rue);
    }

    @Override
    public int hashCode() {
        return this.idPersonne;
    }

}
