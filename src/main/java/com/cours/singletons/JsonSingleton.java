/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cours.singletons;

import com.cours.entities.Personne;
import com.cours.utils.Constants;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.IOException;
import java.util.ArrayList;


public class JsonSingleton extends AbstractSingleton {
    private static final Log log = LogFactory.getLog(JsonSingleton.class);
    protected static JsonSingleton instance = null;
    final String personnesJsonPathFile = Constants.PERSONNES_JSON_PATH_FILE;

    private JsonSingleton() {
        this.extractPersonnesDatas();
    }

    public static JsonSingleton getInstance(){
        if (instance == null) {
            instance = new JsonSingleton();
        }
        return instance;
    }

    public Personne createPersonneWithFileObject(JSONObject json) {
        return new Personne(Integer.valueOf(String.valueOf(json.get("id"))),
                String.valueOf(json.get("prenom")),
                String.valueOf(json.get("nom")),
                Double.valueOf(String.valueOf(json.get("poids"))),
                Double.valueOf(String.valueOf(json.get("taille"))),
                String.valueOf(json.get("rue")),
                String.valueOf(json.get("ville")),
                String.valueOf(json.get("codePostal")));
    }

    @Override
    protected void extractPersonnesDatas() {
        //JSON from file to Object
        JSONParser parser = new JSONParser();
        personnes = new ArrayList<>();
        try{
            Object obj = parser.parse(getPersonnesData(personnesJsonPathFile));
            JSONObject job = (JSONObject)obj;
            JSONArray array = (JSONArray) job.get("personnes");
            for (Object json: array) {
                personnes.add(createPersonneWithFileObject((JSONObject)json));
            }
        }
        catch (IOException | org.json.simple.parser.ParseException e){
            log.error(e.getMessage());
        }
    }
}
