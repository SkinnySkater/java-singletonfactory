/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cours.singletons;

import com.cours.entities.Personne;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;


public abstract class AbstractSingleton {
    protected List<Personne> personnes = null;

    public List<Personne> getPersonnes() {
        return personnes;
    }

    protected String getPersonnesData(String filePath) throws IOException
    {
        return new String(Files.readAllBytes(Paths.get(filePath)));
    }

    protected abstract void extractPersonnesDatas();
}
