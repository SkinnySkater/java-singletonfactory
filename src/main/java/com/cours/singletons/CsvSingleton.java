/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cours.singletons;

import com.cours.entities.Personne;
import com.cours.utils.Constants;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.util.ArrayList;


public class CsvSingleton extends AbstractSingleton {
    private static final Log log = LogFactory.getLog(CsvSingleton.class);
    protected static CsvSingleton instance = null;
    final String personnesCsvPathFile = Constants.PERSONNES_CSV_PATH_FILE;

    private CsvSingleton() {
        this.extractPersonnesDatas();
    }

    public static CsvSingleton getInstance(){
        if (instance == null) {
            instance = new CsvSingleton();
        }
        return instance;
    }

    private Personne createPersonneWithFileObject(String[] attr) {
        return new Personne(Integer.valueOf(attr[0]), attr[1], attr[2], Double.valueOf(attr[3])
                , Double.valueOf(attr[4]), attr[5], attr[6], attr[7]);
    }

    @Override
    protected void extractPersonnesDatas() {
        String loadedFile = null;
        this.personnes = new ArrayList<>();
        try{
            loadedFile = getPersonnesData(personnesCsvPathFile);
        }
        catch (IOException e){
            log.error(e.getMessage());
        }
        //remove the first , informative, line
        loadedFile = loadedFile.substring(loadedFile.indexOf('\n') + 1);
        for (String sline: loadedFile.split("\n")) {
            sline = sline.replaceAll("\\s+","");
            personnes.add(createPersonneWithFileObject(sline.split(Constants.CSV_SEPARATOR)));
        }
    }
}
