/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cours.singletons;

import com.cours.entities.Personne;
import com.cours.utils.Constants;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;


public class XmlSingleton extends AbstractSingleton {
    private static final Log log = LogFactory.getLog(XmlSingleton.class);
    protected static XmlSingleton instance = null;
    final String personnesXmlPathFile = Constants.PERSONNES_XML_PATH_FILE;

    private XmlSingleton() {
        this.extractPersonnesDatas();
    }

    public static XmlSingleton getInstance(){
        if (instance == null) {
            instance = new XmlSingleton();
        }
        return instance;
    }

    public Personne createPersonneWithFileObject(Element elmt) {
        return new Personne(Integer.valueOf(elmt.getAttributes().getNamedItem("id").getNodeValue()),
                elmt.getElementsByTagName("prenom").item(0).getChildNodes().item(0).getNodeValue(),
                elmt.getElementsByTagName("nom").item(0).getChildNodes().item(0).getNodeValue(),
                Double.valueOf(elmt.getElementsByTagName("poids").item(0).getChildNodes().item(0).getNodeValue()),
                Double.valueOf(elmt.getElementsByTagName("taille").item(0).getChildNodes().item(0).getNodeValue()),
                elmt.getElementsByTagName("rue").item(0).getChildNodes().item(0).getNodeValue(),
                elmt.getElementsByTagName("ville").item(0).getChildNodes().item(0).getNodeValue(),
                elmt.getElementsByTagName("codePostal").item(0).getChildNodes().item(0).getNodeValue());
    }

    @Override
    protected void extractPersonnesDatas() {
        String loadedFile = null;
        personnes = new ArrayList<>();
        try{
            loadedFile = getPersonnesData(personnesXmlPathFile);
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            ByteArrayInputStream input = new ByteArrayInputStream(loadedFile.getBytes("UTF-8"));
            Document doc = builder.parse(input);
            NodeList nList = doc.getDocumentElement().getChildNodes();
            for (int i = 0; i < nList.getLength(); i++){
                Node nNode = nList.item(i);
                if (nNode.getNodeType() == Node.ELEMENT_NODE)
                    personnes.add(createPersonneWithFileObject((org.w3c.dom.Element)nNode));
            }
        }
        catch (IOException | ParserConfigurationException | SAXException e){
            log.error(e.getMessage());
        }
    }

}
